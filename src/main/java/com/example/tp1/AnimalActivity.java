package com.example.tp1;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);
        String Titre, Img, Esp, PAdul, PNaiss, Statut, Gest;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null)
            {
                Titre = null;
                Img = null;
                Esp = null;
                PAdul = null;
                PNaiss = null;
                Statut = null;
                Gest = null;
            } else {
                Titre = extras.getString("name");
                Img = extras.getString("img");
                Esp = extras.getString("esperance");
                PAdul = extras.getString("pAdult");
                PNaiss = extras.getString("pNaiss");
                Statut = extras.getString("statut");
                Gest = extras.getString("gestation");
            }
        } else {
            Titre = (String) savedInstanceState.getSerializable("name");
            Img = (String) savedInstanceState.getSerializable("img");
            Esp = (String) savedInstanceState.getSerializable("esperance");
            PAdul = (String) savedInstanceState.getSerializable("pAdult");
            PNaiss = (String) savedInstanceState.getSerializable("pNaiss");
            Statut = (String) savedInstanceState.getSerializable("statut");
            Gest = (String) savedInstanceState.getSerializable("gestation");
        }
        TextView titre = (TextView) findViewById(R.id.Titre);
        titre.setText(Titre);
        TextView esp = (TextView) findViewById(R.id.Esp);
        esp.setText(Esp);
        TextView pAdul = (TextView) findViewById(R.id.PAdul);
        pAdul.setText(PAdul);
        TextView pNaiss = (TextView) findViewById(R.id.PNaiss);
        pNaiss.setText(PNaiss);
        TextView statut = (TextView) findViewById(R.id.Statut);
        statut.setText(Statut);
        TextView gest = (TextView) findViewById(R.id.Gest);
        gest.setText(Gest);
        String draw = "@drawable/"+Img;
        int imageResource = getResources().getIdentifier(draw, null, getPackageName());
        ImageView imageView = (ImageView)findViewById(R.id.image);
        Drawable res = getResources().getDrawable(imageResource);
        imageView.setImageDrawable(res);
        final AnimalList animalListe = new AnimalList();
        final Animal animal = animalListe.getAnimal(Titre);
        final Button button = findViewById(R.id.Save);
        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                final EditText et = (EditText) findViewById(R.id.Statut);
                String statut2 = et.getText().toString();
                animal.setConservationStatus(statut2);
            }
        });
    }
}
