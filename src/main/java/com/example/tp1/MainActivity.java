package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;




public class MainActivity extends AppCompatActivity {

    private ListView listview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview = (ListView) findViewById(R.id.lstView);
        final AnimalList animalListe = new AnimalList();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, animalListe.getNameArray());
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(MainActivity.this, AnimalActivity.class);
                final String item = (String) parent.getItemAtPosition(position);
                Animal animal = animalListe.getAnimal(item);
                intent.putExtra("name", item);
                intent.putExtra("esperance", animal.getHightestLifespan() + " années");
                intent.putExtra("gestation", animal.getGestationPeriod() + " jours");
                intent.putExtra("pNaiss", animal.getBirthWeight() + " kg");
                intent.putExtra("pAdult", animal.getAdultWeight() + " kg");
                intent.putExtra("statut", animal.getConservationStatus());
                intent.putExtra("img", animal.getImgFile());
                startActivity(intent);
            }
        });
    }
}
